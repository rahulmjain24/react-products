import React from 'react'
import "./NavBar.css"
import { Link } from 'react-router-dom';

class NavBar extends React.Component {
    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="container">
                    <Link to="/">
                        <span className="navbar-brand" >Products</span>
                    </Link>
                    <ul className="navbar-nav me-auto">
                        <li className="nav-item">
                            <Link className="nav-link active" aria-current="page" to="/carts">Carts</Link>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}

export default NavBar
