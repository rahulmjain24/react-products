import React from 'react'
import "./FlexContainer.css"

class FlexContainer extends React.Component {
    state = {  } 
    render() { 
        return (
            <div className="d-flex flex-wrap justify-content-center p-2 mx-auto container min-height">{this.props.children}</div>
        );
    }
}
 
export default FlexContainer