import React from 'react'
import "./Card.css"
import { Link } from 'react-router-dom'


class Card extends React.Component {
	render() {
		return (
			<Link to={`/${this.props.id}`} className="d-flex flex-column bg-white overflow-hidden rounded product">
					<div className="flex-shrink-0 bg-white p-2 overflow-hidden img-div">
						<img className="image" src={this.props.image} alt="Product" />
					</div>
					<div className="d-flex flex-column justify-content-between flex-grow-1 m-3">
						<div className="d-flex justify-content-between">
							<span className="rate">
								<img className="me-1 icon" src="img/star.png" alt="star" />{this.props.rating.rate}
							</span>
							<span className="count">
								<img className="me-1 icon" src="img/user.png" alt="user" />{this.props.rating.count}
							</span>
						</div>
						<div className="title-info">
							<p className="title">{this.props.title}</p>
							<p className="text-secondary text-capitalize category">{this.props.category}</p>
						</div>
						<div className="d-flex justify-content-between align-items-center">
							<p className="d-inline text-dark h2">${this.props.price}</p>
						</div>
						
					</div>
				</Link>
		)
	}
}

export default Card