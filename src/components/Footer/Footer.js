import React from 'react'
import "./Footer.css"

class Footer extends React.Component {
    render() { 
        return (
            <footer className='bg-white p-3 border'>
                <p className="main text-dark">Products</p>
                <p className="sub text-dark">&copy; Rahul Jain, All rights reserved</p>
            </footer>
        )
    }
}
 
export default Footer