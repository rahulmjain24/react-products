import React from 'react'
import "./CartItem.css"
import { Link } from 'react-router-dom'

class CartItem extends React.Component {
    render() { 
        return (
            <Link to={`/${this.props.id}`} className="d-flex justify-content-start item-con rounded">
                <div className="cart-image d-inline-block flex-shrink-0">
                    <img src={this.props.image} alt="product" />
                </div>
                <div className="d-flex flex-column justify-content-between">
                     <div className="title-info">
						<p className="title">{this.props.title}</p>
						<p className="text-secondary text-capitalize category">{this.props.category}</p>
					</div>
                    <p className="d-inline text-dark h2">${this.props.price}</p>
                    <div className="text-dark">Quantity: {this.props.quantity}</div>
                </div>
            </Link>
        )
    }
}
 
export default CartItem