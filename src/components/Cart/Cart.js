import React from 'react'
import axios from 'axios'
import Error from '../Error/Error'
import Loader from '../Loader/Loader'
import CartItem from './CatItem'
import CartHeading from './CartHeading'
import FlexContainer from '../FlexContainer/FlexContainer'

class Cart extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            carts: [],
            // users: [],
            error: false
        }
        this.CARTS = 'https://fakestoreapi.com/carts/'
        this.USERS = "https://fakestoreapi.com/users/"
        this.PRODUCTS = "https://fakestoreapi.com/products/"
    }

    getByUrl = (url) => {
        return axios.get(url).then((response) => {
            return response.data
        })
            .catch((err) => {
                console.error(err)
                this.setState({ error: true })
            })
    }

    componentDidMount() {
        this.getByUrl(this.CARTS).then((data) => {
            return Promise.all(data.map((cartItem) => {
                return this.getByUrl(this.USERS + cartItem.userId).then((userData) => {
                    delete cartItem.userId
                    cartItem.user = userData
                    return cartItem
                })
                    .then((cartItem) => {
                        return Promise.all(cartItem.products.map((product) => {
                            return this.getByUrl(this.PRODUCTS + product.productId).then((data) => {
                                delete product.productId
                                product.product = data
                                return product
                            })
                        }))
                            .then((products) => {
                                cartItem.products = products
                                return cartItem
                            })
                    })
            }))
        })
            .then((data) => {
                this.setState({ carts: data })
            })
    }

    render() {
        return (
            <FlexContainer>
                {
                    this.state.error ?
                        <Error>Something went wrong</Error>
                        :
                        this.state.carts.length === 0 ?
                            <Loader />
                            :
                            this.state.carts.map((cartItem, index) => {
                                return (
                                    <CartHeading key={index} {...cartItem.user}>
                                        {cartItem.products.map((item) => {
                                            return <CartItem key={item.product.id} {...item.product} quantity={item.quantity}/>
                                        })}
                                    </CartHeading>
                                )
                            })
                }
            </FlexContainer>
        )
    }
}

export default Cart