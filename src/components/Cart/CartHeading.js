import React from 'react'
import "./CartHeading.css"

class CartHeading extends React.Component {
    render() { 
        return (
            <div className='cart-heading'>
                <p className="name rounded">{this.props.name.firstname.toUpperCase()} {this.props.name.lastname.toUpperCase()}<span className="user">({this.props.username})</span></p>
                {this.props.children}
            </div>
        )
    }
}
 
export default CartHeading