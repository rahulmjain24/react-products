import React from 'react'
import Error from '../Error/Error';
import FlexContainer from '../FlexContainer/FlexContainer';

class PageNotFound extends React.Component {
    state = {  } 
    render() { 
        return (
            <FlexContainer>
                <Error>404 Page not found!!</Error>
            </FlexContainer>
        )
    }
}
 
export default PageNotFound;