import React from 'react'
import axios from 'axios'
import FlexContainer from '../FlexContainer/FlexContainer'
import Error from '../Error/Error'
import Loader from '../Loader/Loader'

import "./Product.css"
import PageNotFound from '../404/404'


class Product extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            product: [],
            error: false
        }
        this.URL = 'https://fakestoreapi.com/products/'
    }

    componentDidMount() {
        axios.get(this.URL + this.props.match.params.id).then((res) => {
                this.setState({ product: res.data })
            })
            .catch((err) => {
                console.error(err)
                this.setState({ error: true })
            })
    }


    render() {
        return (
            <FlexContainer>
                {
                    this.state.error ?
                        <Error>Something went wrong</Error>
                        :
                        typeof this.state.product === 'string' ?
                            <PageNotFound />
                            :
                            this.state.product.length === 0 ?
                                <Loader />
                                :
                                <div className="container-lg min-height-product rounded">
                                    <img src="img/arrow.svg" alt="" height="50px" className='m-2 back' onClick={() => {
                                        this.props.history.goBack()
                                    }} />
                                    <div className="d-flex align-items-start con">
                                        <div className="img p-1 overflow-hidden rounded bg-white flex-shrink-0">
                                            <img src={this.state.product.image} alt="product" height="300px" width="300px" />
                                        </div>
                                        <div className="info d-flex flex-column justify-content-between p-3">
                                            <div className="title-info">
                                                <p className="title h3">{this.state.product.title}</p>
                                                <p className="text-secondary text-capitalize category">{this.state.product.category}</p>
                                                <div className="d-flex justify-content-between">
                                                    <span className="rate">
                                                        <img className="me-1 icon" src="img/star.png" alt="star" />{this.state.product.rating.rate}
                                                    </span>
                                                    <span className="count">
                                                        <img className="me-1 icon" src="img/user.png" alt="user" />{this.state.product.rating.count}
                                                    </span>
                                                </div>
                                                <p className="descreption">{this.state.product.description}</p>
                                            </div>
                                            <div className="d-flex justify-content-between align-items-center">
                                                <p className="d-inline text-dark h2">${this.state.product.price}</p>
                                                {/* <button className="btn btn-outline-primary">Add to cart</button> */}
                                            </div>
                                        </div>
                                    </div>

                                </div>
                }
            </FlexContainer>
        )
    }
}

export default Product