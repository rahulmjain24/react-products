import React from 'react'
import './App.css'
import axios from 'axios'
import Card from './components/Card/Card'
import Error from './components/Error/Error'
import Loader from './components/Loader/Loader'
import FlexContainer from './components/FlexContainer/FlexContainer'
import NavBar from './components/NavBar/NavBar'
import Footer from './components/Footer/Footer'
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom'
import Product from './components/Product/Product'
import Cart from './components/Cart/Cart'
import PageNotFound from './components/404/404'

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      products: [],
      error: false
    }

    this.URL = 'https://fakestoreapi.com/products'
  }

  getByUrl = (url) => {
    return axios.get(url).then((response) => {
        return response.data
      })
      .catch((err) => {
        console.error(err)
        this.setState({ error: true })
      })
  }

  componentDidMount() {
    this.getByUrl(this.URL).then((data) => {
      this.setState({ products: data })
    })
  }

  Products = () => {
    return (
      <FlexContainer>
        {
          this.state.error ?
            <Error>Something went wrong</Error>
            :
            this.state.products.length === 0 ?
              <Loader />
              :
              this.state.products.map((product) => {
                return (
                  <Card
                    {...product}
                    key={product.id}
                  />
                )
              })
        }
      </FlexContainer>
    )
  }

  render() {

    return (
      <div className="App">
        <Router>
          <NavBar />
          <Switch>
            <Route exact path="/" component={this.Products} />
            <Route exact path="/carts" component={Cart} />
            <Route exact path="/:id" component={Product} />
            <Route path="/" component={PageNotFound} />
          </Switch>
        </Router>
        <Footer />
      </div>
    );
  }
}

export default App
